<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    $nama = $_POST['nama'];
    $jenis_kelamin = $_POST['jenis_kelamin'];
    $agama = $_POST['agama'];

    // Generate greeting message
    $greeting = "Hello, $nama!";

    // Output the greeting message
    echo "<h2>$greeting</h2>";
    echo "<p>Your gender is $jenis_kelamin and your religion is $agama.</p>";
} else {
    echo "Form submission method not allowed!";
}
?>
